unit dmMainUnit;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.IB,
  FireDAC.Phys.IBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.Comp.UI, FireDAC.Phys.FBDef, FireDAC.Phys.IBBase,
  FireDAC.Phys.FB, Vcl.ImgList, Vcl.Controls, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack,
  IdIntercept;

type
  TParams = record
    StationID: Integer;
    StationName: string;
    DBPath: string;
    ScaleIP: string;
    ScaleIPPort: integer;
    ScannerPort: string;
    ScannerBaudRate: integer;
    ScannerRmvFirsChar: Boolean;
    ScanWorker: Boolean;
  end;

  TWorkerInfo = record
     ID: string;
     Name: string;
     Surname: string;
     JobID: integer;
     BarCode: string;
     Procedure Reset;
  end;

  TBoxInfo = record
     Weight: Extended;
     BarCode: string;
     WeightState: string;
     procedure Reset;
  end;



  TWeightState = (wsConnected, wsDisConnected);
  TBarcodeType = (btWorker,btBox, btUnknown);

type
  TdmMain = class(TDataModule)
    FDConnection1: TFDConnection;
    FDTransaction1: TFDTransaction;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDQStation: TFDQuery;
    FDQStationSTATIONID: TIntegerField;
    FDQStationNAME: TStringField;
    FDQStationDESCRIPTION: TStringField;
    FDUpdateSQLStation: TFDUpdateSQL;
    DSoStation: TDataSource;
    FDPhysIBDriverLink1: TFDPhysIBDriverLink;
    FDQMainParams: TFDQuery;
    FDUpdateSQLMainParams: TFDUpdateSQL;
    DSoMainParams: TDataSource;
    FDQCheckParams: TFDQuery;
    FDQMainParamsPARAMETERNAME: TStringField;
    FDQMainParamsPARAMETERVALUE: TStringField;
    FDQLogView: TFDQuery;
    FDQLogViewLOGID: TIntegerField;
    FDQLogViewWEIGHT: TCurrencyField;
    FDQLogViewJOBID: TIntegerField;
    FDQLogViewWEIGHTDATETIME: TSQLTimeStampField;
    FDQLogViewWORKER_BARCODE: TStringField;
    FDQLogViewBOX_BARCODE: TStringField;
    FDQLogViewSTATIONID: TIntegerField;
    FDQLogViewCOLLECTED: TSmallintField;
    DSoLog: TDataSource;
    ImageList1: TImageList;
    IdTCPClient1: TIdTCPClient;
    FDQWorker: TFDQuery;
    FDQWorkerID: TStringField;
    FDQWorkerNAME: TStringField;
    FDQWorkerSURNAME: TStringField;
    FDQWorkerBARCODEID: TStringField;
    FDQWorkerJOBID: TIntegerField;
    FDCommand1: TFDCommand;
    FDQGetGenID: TFDQuery;
    FDUpdateSQLLog: TFDUpdateSQL;
    FDQLogInsert: TFDQuery;
    FDQLogInsertLOGID: TIntegerField;
    FDQLogInsertWEIGHT: TCurrencyField;
    FDQLogInsertJOBID: TIntegerField;
    FDQLogInsertWEIGHTDATETIME: TSQLTimeStampField;
    FDQLogInsertWORKER_BARCODE: TStringField;
    FDQLogInsertBOX_BARCODE: TStringField;
    FDQLogInsertSTATIONID: TIntegerField;
    FDQLogInsertCOLLECTED: TSmallintField;
    IdIOHandlerStack1: TIdIOHandlerStack;
    FDQLogInsertWEIGTSTATE: TStringField;
    FDQLogViewWEIGTSTATE: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure IdTCPClient1Connected(Sender: TObject);
    procedure IdTCPClient1Disconnected(Sender: TObject);  private
  private
    procedure LoadIniFile;
    procedure ConnectDatabase;
    function CheckIfParamExists(ParamName: string):boolean;
    procedure CreateParam(ParamName: string);
    procedure ReadParams;
    function GetGenID (GeneratorNavn : string) : integer;
  public
    Params: TParams;
    WeightState: TWeightState;
    procedure CheckParamTable;
    function GetWorkerFromBarcode(Barcode: string):TWorkerInfo;
    function InsertLogData(WorkerInfo: TWorkerInfo; BoxInfo: TBoxInfo): Boolean;
  end;

var
  dmMain: TdmMain;

implementation

uses
  Vcl.Dialogs, TeraFunkUnit;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

function TdmMain.CheckIfParamExists(ParamName: string): boolean;
begin
    if FDQCheckParams.Active then
       FDQCheckParams.Close;

    FDQCheckParams.ParamByName('PARAMNAME').Value:=ParamName;
    FDQCheckParams.Open;

    if FDQCheckParams.RecordCount = 1 then
       result:=True
    else
       Result:=False;
end;

procedure TdmMain.CheckParamTable;
begin
    if not CheckIfParamExists('ScaleIP') then
       CreateParam('ScaleIP');
    if not CheckIfParamExists('ScaleIPPort') then
       CreateParam('ScaleIPPort');
    if not CheckIfParamExists('BarCodeScanPort') then
       CreateParam('BarCodeScanPort');
    if not CheckIfParamExists('BarCodeScanBaudRate') then
       CreateParam('BarCodeScanBaudRate');
    if not CheckIfParamExists('BarCodeScanDataBit') then
       CreateParam('BarCodeScanDataBit');
    if not CheckIfParamExists('BarCodeScanStopBit') then
       CreateParam('BarCodeScanStopBit');
    if not CheckIfParamExists('BarCodeScanRmvFirstChar') then
       CreateParam('BarCodeScanRmvFirstChar');
    if not CheckIfParamExists('ScanWorker') then
       CreateParam('ScanWorker');

    ReadParams;
end;

procedure TdmMain.ConnectDatabase;
begin
   if FDConnection1.Connected then
      FDConnection1.Close;

   FDConnection1.Params.Database:=Params.DBPath;
   FDConnection1.Connected:=True;
end;


procedure TdmMain.CreateParam(ParamName: string);
begin
    if not FDQMainParams.Active then
       FDQMainParams.Open;

    FDQMainParams.Insert;
    FDQMainParamsPARAMETERNAME.Value:=ParamName;
    FDQMainParams.Post;
end;

procedure TdmMain.DataModuleCreate(Sender: TObject);
begin
   WeightState:=wsDisConnected;
   LoadIniFile;
   ConnectDatabase;
   CheckParamTable;
   ReadParams;
end;


function TdmMain.GetWorkerFromBarcode(Barcode: string): TWorkerInfo;
begin
    Barcode:=Copy(Barcode,1,13);

    if FDQWorker.Active then
       FDQWorker.Close;

    FDQWorker.ParamByName('BARCODE').Value:=Barcode;
    FDQWorker.Open;

    if FDQWorker.RecordCount > 0 then
    begin
        result.ID:=FDQWorkerID.Value;
        result.Name:=FDQWorkerNAME.Value;
        result.Surname:=FDQWorkerSURNAME.Value;
        result.JobID:=FDQWorkerJOBID.Value;
        result.BarCode:=Barcode;
    end;
    FDQWorker.Close;
end;

procedure TdmMain.IdTCPClient1Connected(Sender: TObject);
begin
    WeightState:=wsConnected;
end;

procedure TdmMain.IdTCPClient1Disconnected(Sender: TObject);
begin
    WeightState:=wsDisConnected;
    idTcpClient1.IOHandler.InputBuffer.clear;
end;

function TdmMain.GetGenID (GeneratorNavn : string) : integer;
begin
   with FDQGetGenID do
   begin
      SQL.Clear;
      SQL.ADD('SELECT GEN_ID ('+GeneratorNavn+', 1) from rdb$database');
      Open;

      // Fields 0 er fyrsta felt � Queryini.
      Result := Fields [0].AsInteger;
   end;
end;


function TdmMain.InsertLogData(WorkerInfo: TWorkerInfo; BoxInfo: TBoxInfo): Boolean;
begin
    if not FDQLogInsert.Active then
       FDQLogInsert.Open;

    FDQLogInsert.Insert;
    FDQLogInsertLOGID .Value:=GetGenID('LOGID_GEN');
    FDQLogInsertWEIGHT.Value:=BoxInfo.Weight;
    FDQLogInsertJOBID.Value:=WorkerInfo.JobID;
    FDQLogInsertWEIGHTDATETIME.AsDateTime:=Now;
    FDQLogInsertWORKER_BARCODE.Value:=WorkerInfo.BarCode;
    FDQLogInsertBOX_BARCODE.Value:=BoxInfo.BarCode;
    FDQLogInsertSTATIONID.Value:=Params.StationID;
    FDQLogInsertCOLLECTED.Value:=0;
    FDQLogInsertWEIGTSTATE.Value:=BoxInfo.WeightState;
    FDQLogInsert.Post;

end;

procedure TdmMain.LoadIniFile;
var
    IniFile: TStringList;
begin
    IniFile:=TStringList.Create;
    try
       IniFile.LoadFromFile('Weighingstation.ini');
       Params.DBPath:=IniFile.Values['DATABASE'];
    finally
       IniFile.Free;
    end;

end;

procedure TdmMain.ReadParams;
begin
    FDQMainParams.Open;

    if FDQMainParams.Locate('PARAMETERNAME','ScaleIP',[])  then
       Params.ScaleIP:= FDQMainParamsPARAMETERVALUE.Value;
    if FDQMainParams.Locate('PARAMETERNAME','ScaleIPPort',[])  then
       Params.ScaleIPPort:= StrToIntDef(FDQMainParamsPARAMETERVALUE.Value,0);
    if FDQMainParams.Locate('PARAMETERNAME','BarCodeScanPort',[])  then
       Params.ScannerPort:= FDQMainParamsPARAMETERVALUE.Value;
    if FDQMainParams.Locate('PARAMETERNAME','BarCodeScanRmvFirstChar',[])  then
       Params.ScannerRmvFirsChar:= StrToBoolDef(FDQMainParamsPARAMETERVALUE.Value, True);
    if FDQMainParams.Locate('PARAMETERNAME','BarCodeScanBaudRate',[])  then
       Params.ScannerBaudRate:= StrToIntDef(FDQMainParamsPARAMETERVALUE.Value, 9600);
    if FDQMainParams.Locate('PARAMETERNAME','ScanWorker',[])  then
       Params.ScanWorker:= StrToBoolDef(FDQMainParamsPARAMETERVALUE.Value, False);


    FDQStation.Open;
    Params.StationID:=FDQStationSTATIONID.Value;
    Params.StationName:=FDQStationNAME.Value;
end;

{ TBarCodes }


{ TWorkerInfo }

procedure TWorkerInfo.Reset;
begin
    ID:='';
    Name:= '';
    Surname:= '';
    JobID:= 0;
    BarCode:= '';
end;

{ TBoxInfo }

procedure TBoxInfo.Reset;
begin
    Weight:=0;
    BarCode:= '';
    WeightState:='';
end;

end.
