unit TReadingThreadUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IdCustomTransparentProxy, IdSocks, IdBaseComponent,
  IdComponent, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack,
  IdTCPConnection, IdTCPClient, IdSync;

type

TReadingThread = class(TThread)
  protected
    FConn: TIdTCPConnection;
    LineNo: integer;
    FLog: Boolean;
    procedure Execute; override;
    procedure DoTerminate; override;
    procedure DoOnTerminate(Sender: TObject);
  public
    constructor Create(AConn: TIdTCPConnection; Log: boolean = False); reintroduce;
  end;

TLog = class(TIdSync)
  protected
    FMsg: String;
    function GetWeight(inString: string): Extended;
    procedure DoSynchronize; override;
  protected
    FLog: Boolean;
  public
    constructor Create(const AMsg: string);
    class procedure AddMsg(const AMsg: String; Log: Boolean);
  end;

implementation

uses WeighingStationUnit, TeraFunkUnit;

constructor TReadingThread.Create(AConn: TIdTCPConnection; Log: boolean = False);
begin
  FLog:= Log;

  if FLog = True then
     TLog.AddMsg('DEBUG: TReadingThread.Create', FLog);

  FConn := AConn;
  inherited Create(False);
  //OnTerminate:=DoOnTerminate;  //WeighingStationForm.CheckIfTerminated;
end;

procedure TReadingThread.Execute;
var
  cmd: string;
  LineNo: integer;
begin
  if FLog = True then
    TLog.AddMsg('DEBUG: TReadingThread.Execute', FLog);
  LineNo:=0;

  while not Terminated do
  begin
    cmd := FConn.IOHandler.ReadLn;

    if FLog = True then
    begin
       inc(LineNo);
       TLog.AddMsg(cmd, FLog);
    end
    else
    begin
       TLog.AddMsg(cmd, FLog);
    end;
  end;
end;



procedure TReadingThread.DoOnTerminate(Sender: TObject);
begin
    ShowMessage('Hey');
end;

procedure TReadingThread.DoTerminate;
begin
  if FLog = True then
      TLog.AddMsg('DEBUG: TReadingThread.DoTerminate', FLog);

  FConn.IOHandler.InputBuffer.Clear;
  FConn.Disconnect;
  inherited;
end;

constructor TLog.Create(const AMsg: string);
begin
  inherited Create;
  FMsg := AMsg;
end;

procedure TLog.DoSynchronize;
var
  Weight: Extended;
begin
    if Copy(FMsg,0,2) = 'ST' then
    begin
        Weight:=GetWeight(FMsg);
        //if Weight > 0 then
        //begin
            with WeighingStationForm do
            begin
                try
                    if FLog = True then
                       MemoScale.Lines.Add(TimeToStr(Time)+': '+FMsg);

                    CurrentWeight:=Weight;
                    LabelCurrentWeight.Caption:=FloatToStrF(CurrentWeight, ffFixed, 6,3);
                    LabelCurrWeigtTime.Caption:=TimeToStr(Time);
                Except
                    on E: Exception do
                    Begin
                        LabelCurrentWeight.Caption:='0.000';
                    End;
                end;
            end;
        //end;
    end;
end;

function TLog.GetWeight(inString: string): Extended;
var
    sWeight: string;
begin
    FormatSettings.DecimalSeparator:='.';
    sWeight:=Trim(getStringFromString(inString,',',2,3));

    if sWeight > '' then
    begin
      try
          result:=StrToFloat(sWeight);
      Except
          //ShowMessage(WeightStr);
          result:=0;
      end;
    end;
end;

class procedure TLog.AddMsg(const AMsg: String; Log: Boolean);
begin
  with Create(AMsg) do
  try
    FLog:=Log;
    Synchronize;
  finally
    Free;
  end;
end;

end.
