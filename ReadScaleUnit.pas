unit ReadScaleUnit;

interface

uses
  System.Classes, ComPort, IdTCPClient, IdIOHandler,dmMainUnit,
  IdIOHandlerStack, vcl.Graphics;

type

TReadScaleThread = class(TThread)
protected
   LineNo: integer;
   TCPConnection: TIdTCPClient;
   IOHandler: TIdIOHandlerStack;
   FLog: Boolean;
   function GetWeight(inString: string): Extended;
   procedure Execute; override;
   procedure DoSynchronize(WeightLog: string);
   procedure SendError;
   procedure Connected(Sender: TObject);
   procedure Disconnected(Sender: TObject);
   function ConnectToScale:Boolean;
   function IsStrANumber(const S: string): Boolean;
public
   constructor Create(Params: TParams; Log:Boolean = False); reintroduce;
end;


implementation

{ TReadScaleThread }

uses WeighingStationUnit, System.SysUtils, TeraFunkUnit;



procedure TReadScaleThread.Connected(Sender: TObject);
begin
    dmMain.WeightState:=wsConnected;
end;

function TReadScaleThread.ConnectToScale: Boolean;
begin
    try
        TCPConnection.Connect;
        Result:=True;
        WeighingStationForm.Timer1.Enabled:=True;
    except
        SendError;
        result:=False;
    end;
end;

constructor TReadScaleThread.Create(Params: TParams; Log:Boolean = False);
begin
   inherited Create(False);
   FLog:=Log;
   TCPConnection:=TIdTCPClient.Create;
   IOHandler:=TIdIOHandlerStack.Create;
   IOHandler.RecvBufferSize:=64;
   TCPConnection.IOHandler:=IOHandler;
   TCPConnection.Host:=Params.ScaleIP;
   TCPConnection.Port:=Params.ScaleIPPort;
   TCPConnection.OnConnected:=Connected;
   TCPConnection.OnDisconnected:=Disconnected;
   ConnectToScale;
end;

procedure TReadScaleThread.Disconnected(Sender: TObject);
begin
   dmMain.WeightState:=wsDisConnected;
end;

function TReadScaleThread.GetWeight(inString: string): Extended;
var
    sWeight: string;
begin
    FormatSettings.DecimalSeparator:='.';
    sWeight:=inString; // Trim(getStringFromString(inString,',',2,3));

    if sWeight > '' then
    begin
      try
          result:=StrToFloat(sWeight);
      Except
          result:=0;
      end;
    end;
end;

function TReadScaleThread.IsStrANumber(const S: string): Boolean;
var
  P: PChar;
begin
    P      := PChar(S);
    Result := False;
    while P^ <> #0 do
    begin
      if not ((P^ in ['0'..'9']) or (P^ = '.') or (P^ = '-')) then
          Exit;

      Inc(P);
    end;
    Result := True;
end;


procedure TReadScaleThread.DoSynchronize(WeightLog: string);
var
  Weight: Extended;
  WeightState: string;
  sWeight: string;
begin
   inc(LineNo);

   if WeighingStationForm.chkBoxDebug.Checked then
      WeighingStationForm.MemoScale.Lines.Add(IntToStr(LineNo)+': '+WeightLog);

   WeighingStationForm.ScaleLineID:=LineNo;
   WeightState:=Copy(WeightLog,0,2);

   if (WeightState = 'ST') or (WeightState = 'US') then
    begin
        sWeight:=Trim(getStringFromString(WeightLog,',',2,3));

        if IsStrANumber(sWeight) = True then
        begin
            Weight:=GetWeight(sWeight);
            with WeighingStationForm do
            begin
                try
                    if FLog = True then
                       MemoScale.Lines.Add(TimeToStr(Time)+': '+WeightLog);

                    CurrentWeight:=Weight;
                    SetCurrentWeightLabels(CurrentWeight, WeightState);
                Except
                    on E: Exception do
                    Begin
                        LabelCurrentWeight.Caption:='0.000';
                    End;
                end;
            end;
        end;
    end;
end;

procedure TReadScaleThread.SendError;
begin
    WeighingStationForm.ScreenMessage('ERROR','Communation with scale', True);
    self.Terminate;
end;

procedure TReadScaleThread.Execute;
var
  WeightLog: string;
begin
    inherited;
    try
       while not Terminated do
       begin
           WeightLog:=TCPConnection.IOHandler.ReadLn;
           DoSynchronize(WeightLog);

           //Sleep(100);
       end;
    except
         SendError;
    end;
end;

end.
