object WeighingStationForm: TWeighingStationForm
  Left = 0
  Top = 0
  Caption = 'Weighing Station'
  ClientHeight = 610
  ClientWidth = 1153
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 113
    Height = 536
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object spdBtnConnect: TSpeedButton
      Left = 4
      Top = 8
      Width = 107
      Height = 100
      AllowAllUp = True
      OnClick = spdBtnConnectClick
    end
    object BitBtn1: TBitBtn
      Left = 4
      Top = 213
      Width = 107
      Height = 100
      Caption = 'Setup'
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object BitBtnMainWindow: TBitBtn
      Left = 4
      Top = 110
      Width = 107
      Height = 100
      Caption = 'Main Window'
      TabOrder = 1
      WordWrap = True
      OnClick = BitBtnMainWindowClick
    end
    object BitBtn4: TBitBtn
      Left = 4
      Top = 316
      Width = 107
      Height = 100
      Caption = 'Log'
      TabOrder = 2
      OnClick = BitBtn4Click
    end
    object BitBtn5: TBitBtn
      Left = 4
      Top = 422
      Width = 107
      Height = 100
      Caption = 'Close Program'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = BitBtn5Click
    end
  end
  object PageControl1: TPageControl
    Left = 113
    Top = 0
    Width = 1040
    Height = 536
    ActivePage = tbsMainWindow
    Align = alClient
    MultiLine = True
    TabOrder = 1
    object tbsMainWindow: TTabSheet
      Caption = 'tbsMainWindow'
      object GroupBox2: TGroupBox
        Left = 18
        Top = 3
        Width = 79
        Height = 76
        Caption = 'Station ID'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 14
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object LabelStationID: TLabel
          Left = 20
          Top = 27
          Width = 41
          Height = 33
          Alignment = taCenter
          AutoSize = False
          Caption = 'ID'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = []
          GlowSize = 100
          ParentColor = False
          ParentFont = False
          Transparent = True
        end
      end
      object GroupBox3: TGroupBox
        Left = 111
        Top = 3
        Width = 410
        Height = 76
        Caption = 'Station name'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 14
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object LabelStationName: TLabel
          Left = 29
          Top = 27
          Width = 356
          Height = 33
          Alignment = taCenter
          AutoSize = False
          Caption = 'ID'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = True
        end
      end
      object GroupBox4: TGroupBox
        Left = 527
        Top = 3
        Width = 488
        Height = 113
        Caption = 'Last weight'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 14
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object LabelLastWeight: TLabel
          Left = 366
          Top = 27
          Width = 100
          Height = 48
          Alignment = taCenter
          Caption = '0.000'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -40
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = True
        end
        object LabelLastWeigtTime: TLabel
          Left = 400
          Top = 82
          Width = 66
          Height = 20
          Caption = '00:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = 20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TLabel
          Left = 16
          Top = 32
          Width = 41
          Height = 19
          Caption = 'Name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LabelLastWorker: TLabel
          Left = 73
          Top = 20
          Width = 187
          Height = 42
          Caption = '.................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -35
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 16
          Top = 79
          Width = 26
          Height = 19
          Caption = 'Box'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LabelLastBox: TLabel
          Left = 73
          Top = 60
          Width = 187
          Height = 42
          Caption = '.................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -35
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object GroupBox5: TGroupBox
        Left = 527
        Top = 119
        Width = 488
        Height = 209
        Caption = 'Current weight'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        object LabelCurrentWeight: TLabel
          Left = 166
          Top = 25
          Width = 264
          Height = 129
          Alignment = taRightJustify
          Caption = '0.000'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -107
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = True
        end
        object LabelCurrWeigtTime: TLabel
          Left = 355
          Top = 165
          Width = 66
          Height = 20
          Caption = '00:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = 20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object GrpBoxWorkerInfo: TGroupBox
        Left = 18
        Top = 331
        Width = 503
        Height = 169
        Caption = 'Workers info'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        object Label2: TLabel
          Left = 16
          Top = 34
          Width = 96
          Height = 33
          Caption = 'Barcode'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 16
          Top = 82
          Width = 69
          Height = 33
          Caption = 'Name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LabelWorkerName: TLabel
          Left = 192
          Top = 67
          Width = 204
          Height = 48
          Caption = '.................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -40
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LabelWorkerBarCode: TLabel
          Left = 192
          Top = 21
          Width = 204
          Height = 48
          Caption = '.................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -40
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LabelWorkerScanTime: TLabel
          Left = 322
          Top = 133
          Width = 66
          Height = 20
          Caption = '00:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBtnText
          Font.Height = 20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object GroupBox8: TGroupBox
        Left = 527
        Top = 331
        Width = 488
        Height = 169
        Caption = 'Box info'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        object LabelBoxBarcode: TLabel
          Left = 234
          Top = 21
          Width = 204
          Height = 48
          Alignment = taRightJustify
          Caption = '.................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -40
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LabelBoxScanTime: TLabel
          Left = 366
          Top = 133
          Width = 66
          Height = 20
          Alignment = taRightJustify
          Caption = '00:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBtnText
          Font.Height = 20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LabelBoxWeight: TLabel
          Left = 234
          Top = 67
          Width = 204
          Height = 48
          Alignment = taRightJustify
          Caption = '.................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -40
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 16
          Top = 34
          Width = 96
          Height = 33
          Caption = 'Barcode'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 16
          Top = 80
          Width = 83
          Height = 33
          Caption = 'Weight'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object GroupBox6: TGroupBox
        Left = 18
        Top = 85
        Width = 503
        Height = 243
        Caption = 'Messages'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 14
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        object LabelInfo1: TLabel
          Left = 20
          Top = 43
          Width = 461
          Height = 90
          Alignment = taCenter
          AutoSize = False
          Caption = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = 90
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LabelInfo2: TLabel
          Left = 5
          Top = 151
          Width = 486
          Height = 74
          Alignment = taCenter
          AutoSize = False
          Caption = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = 50
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object tbsSetup: TTabSheet
      Caption = 'tbsSetup'
      ImageIndex = 1
      object DBGrid1: TDBGrid
        Left = 3
        Top = 216
        Width = 350
        Height = 210
        DataSource = dmMain.DSoMainParams
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'PARAMETERNAME'
            Title.Caption = 'Parameter'
            Width = 148
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PARAMETERVALUE'
            Title.Caption = 'Parameter value'
            Width = 168
            Visible = True
          end>
      end
      object GroupBox1: TGroupBox
        Left = 3
        Top = 3
        Width = 222
        Height = 169
        Caption = 'Station ID'
        TabOrder = 1
        object DBEdit1: TDBEdit
          Left = 16
          Top = 59
          Width = 121
          Height = 21
          DataField = 'STATIONID'
          DataSource = dmMain.DSoStation
          TabOrder = 0
          OnEnter = DBEdit1Enter
        end
        object DBEdit2: TDBEdit
          Left = 16
          Top = 86
          Width = 193
          Height = 21
          DataField = 'NAME'
          DataSource = dmMain.DSoStation
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 16
          Top = 113
          Width = 193
          Height = 21
          DataField = 'DESCRIPTION'
          DataSource = dmMain.DSoStation
          TabOrder = 2
        end
        object DBNavigator1: TDBNavigator
          Left = 16
          Top = 21
          Width = 120
          Height = 25
          DataSource = dmMain.DSoStation
          VisibleButtons = [nbDelete, nbEdit, nbPost, nbCancel]
          TabOrder = 3
        end
      end
      object BitBtn2: TBitBtn
        Left = 377
        Top = 216
        Width = 75
        Height = 65
        Caption = 'Refresh'
        TabOrder = 2
        OnClick = BitBtn2Click
      end
      object chkBoxDebug: TCheckBox
        Left = 248
        Top = 7
        Width = 97
        Height = 17
        Caption = 'Debug'
        TabOrder = 3
        OnClick = chkBoxDebugClick
      end
    end
    object tbsLogView: TTabSheet
      Caption = 'tbsLogView'
      ImageIndex = 2
      object DBGrid2: TDBGrid
        Left = 0
        Top = 56
        Width = 1032
        Height = 452
        Align = alClient
        DataSource = dmMain.DSoLog
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1032
        Height = 56
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn3: TBitBtn
          Left = 0
          Top = 1
          Width = 75
          Height = 52
          Caption = 'Show log'
          TabOrder = 0
          OnClick = BitBtn3Click
        end
        object ChkBoxShowClosed: TCheckBox
          Left = 128
          Top = 17
          Width = 97
          Height = 17
          Caption = 'Show closed'
          TabOrder = 1
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      ImageIndex = 3
      DesignSize = (
        1032
        508)
      object MemoScale: TMemo
        Left = 3
        Top = 35
        Width = 230
        Height = 420
        Anchors = [akLeft, akTop, akBottom]
        TabOrder = 0
      end
      object Button4: TButton
        Left = 3
        Top = 3
        Width = 75
        Height = 25
        Caption = 'Read Buffer'
        TabOrder = 1
        OnClick = Button4Click
      end
      object MemoBarcode: TMemo
        Left = 239
        Top = 35
        Width = 185
        Height = 246
        TabOrder = 2
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 536
    Width = 1153
    Height = 74
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object EditBarCode: TEdit
      Left = 457
      Top = 19
      Width = 322
      Height = 38
      BevelInner = bvNone
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHotLight
      Font.Height = 30
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnKeyPress = EditBarCodeKeyPress
    end
  end
  object ComPort1: TComPort
    Active = False
    BaudRate = brCustom
    CustomBaudRate = 9600
    DataBits = db8
    DeviceName = 'COM7'
    Options = []
    StopBits = sb1
    Left = 381
    Top = 552
  end
  object ComSignal1: TComSignal
    Delay = 100
    Signal = siNone
    Left = 325
    Top = 552
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 261
    Top = 552
  end
end
