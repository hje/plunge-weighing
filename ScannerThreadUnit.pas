unit ScannerThreadUnit;


interface

uses
  System.Classes, ComPort;

type

TScannerThread = class(TThread)
protected
   FComPort: TComPort;
   procedure Execute; override;
   procedure DoSynchronize(sBarCode: AnsiString);
public
   constructor Create(ComPort: TComPort); reintroduce;
end;

implementation

uses
  System.SysUtils, WeighingStationUnit;

constructor TScannerThread.Create(ComPort: TComPort);
begin
    FComPort:=ComPort;
    inherited Create(False);
end;

procedure TScannerThread.DoSynchronize(sBarCode: AnsiString);
var
  KeyReturn: char;
begin
  inherited;
    KeyReturn:=char(#13);
    WeighingStationForm.EditBarCode.Text:=string(sBarCode)+char(#13);
    //WeighingStationForm.Memo1.Lines.Add(string(sBarCode));
    WeighingStationForm.EditBarCodeKeyPress(self,KeyReturn);
    //Beep;
          //Terminate;
end;

procedure TScannerThread.Execute;
var
  inputChar: AnsiChar;
  sBarCode: AnsiString;
begin
   while not Terminated do
   begin
      inputChar:=FComPort.ReadAnsiChar;
      sBarCode:=sBarCode+inputChar;

      if inputChar = char(#13) then
      begin
          DoSynchronize(sBarCode);
          sBarCode:='';
      end;
   end;
end;

end.
