program WeighingStation;

uses
  Vcl.Forms,
  WeighingStationUnit in 'WeighingStationUnit.pas' {WeighingStationForm},
  dmMainUnit in 'dmMainUnit.pas' {dmMain: TDataModule},
  TeraFunkUnit in 'TeraFunkUnit.pas',
  ScannerThreadUnit in 'ScannerThreadUnit.pas',
  ReadScaleUnit in 'ReadScaleUnit.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TWeighingStationForm, WeighingStationForm);
  Application.Run;
end.
