unit WeighingStationUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, IdCustomTransparentProxy,
  IdSocks, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, Math, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, Vcl.Grids, Vcl.DBGrids, FireDAC.Comp.UI,
  FireDAC.Comp.Client, Data.DB, Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.Mask, Vcl.DBCtrls, TReadingThreadUnit, ComSignal, ComPort, dmMainUnit,
  ScannerThreadUnit, ReadScaleUnit,System.Character;

type
  TBarCodeScanned = record
    BoxScanned: Boolean;
    WorkerScanned: Boolean;
    procedure Reset;
    function AllScanned: Boolean;
  end;

type
  TWeighingStationForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    PageControl1: TPageControl;
    tbsMainWindow: TTabSheet;
    tbsSetup: TTabSheet;
    DBGrid1: TDBGrid;
    GroupBox1: TGroupBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBNavigator1: TDBNavigator;
    BitBtnMainWindow: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox2: TGroupBox;
    LabelStationID: TLabel;
    tbsLogView: TTabSheet;
    DBGrid2: TDBGrid;
    Panel2: TPanel;
    BitBtn3: TBitBtn;
    ChkBoxShowClosed: TCheckBox;
    BitBtn4: TBitBtn;
    GroupBox3: TGroupBox;
    LabelStationName: TLabel;
    GroupBox4: TGroupBox;
    LabelLastWeight: TLabel;
    GroupBox5: TGroupBox;
    LabelCurrentWeight: TLabel;
    spdBtnConnect: TSpeedButton;
    LabelCurrWeigtTime: TLabel;
    LabelLastWeigtTime: TLabel;
    TabSheet1: TTabSheet;
    MemoScale: TMemo;
    chkBoxDebug: TCheckBox;
    Button4: TButton;
    ComPort1: TComPort;
    ComSignal1: TComSignal;
    MemoBarcode: TMemo;
    GrpBoxWorkerInfo: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    LabelWorkerName: TLabel;
    LabelWorkerBarCode: TLabel;
    GroupBox8: TGroupBox;
    LabelBoxBarcode: TLabel;
    LabelWorkerScanTime: TLabel;
    LabelBoxScanTime: TLabel;
    LabelBoxWeight: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Panel3: TPanel;
    EditBarCode: TEdit;
    Timer1: TTimer;
    Label1: TLabel;
    LabelLastWorker: TLabel;
    GroupBox6: TGroupBox;
    LabelInfo1: TLabel;
    LabelInfo2: TLabel;
    Label4: TLabel;
    LabelLastBox: TLabel;
    BitBtn5: TBitBtn;
    procedure DBEdit1Enter(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtnMainWindowClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure spdBtnConnectClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure chkBoxDebugClick(Sender: TObject);
    procedure EditBarCodeKeyPress(Sender: TObject; var Key: Char);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BitBtn5Click(Sender: TObject);
  private
    LineNo: integer;
    //inputString: AnsiString;
    LastScaleLineID: integer;
    BarCodeScanned: TBarCodeScanned;
    WorkerInfo: TWorkerInfo;
    BoxInfo: TBoxInfo;
    procedure SetLabels;
    function SetComPort:Boolean;
    function BarcodeFirstChar(Barcode: string):string;
    function GetBarCodeType(Barcode: string):TBarcodeType;
    procedure HandleBarecode(Barcode: string);
  public
    CurrentWeight: Extended;
    ScaleLineID: integer;
    procedure SetCurrentWeightLabels(Weight: Extended; WeightState: string);
    procedure SetLastWeightLabels(WorkerInfo: TWorkerInfo; BoxInfo: TBoxInfo);
    procedure ScreenMessage(Line1, Line2: string; msgError: Boolean);
  end;

var
  WeighingStationForm: TWeighingStationForm;
  ReadScaleTr: TReadScaleThread = nil;
  ScanTr: TScannerThread = nil;

implementation

{$R *.dfm}

uses IdGlobal ;

function TWeighingStationForm.BarcodeFirstChar(Barcode: string): string;
var
  FirstChar: char; //TCharacter;
begin
    with dmMain do
    begin
       if params.ScannerRmvFirsChar = True then
       begin
          FirstChar:=Barcode[1];
          if FirstChar.isLetter then
             result:=copy(Barcode,2,length(Barcode)-1)
       end
       else
          result:=Barcode;
    end;
end;

procedure TWeighingStationForm.BitBtn1Click(Sender: TObject);
begin
    tbsSetup.Show;
end;

procedure TWeighingStationForm.BitBtn2Click(Sender: TObject);
begin
    with dmMain do
    begin
        checkparamtable;

        if not FDQMainParams.Active then
           FDQMainParams.Open;
    end;

    SetLabels;
end;

procedure TWeighingStationForm.BitBtn3Click(Sender: TObject);
begin
    with dmMain do
    begin
       if FDQLogView.Active then
          FDQLogView.Close;

       if ChkBoxShowClosed.Checked then
          FDQLogView.ParamByName('COLLECTED').Value:=1
       else
          FDQLogView.ParamByName('COLLECTED').Value:=0;

       FDQLogView.Open;
    end;
end;

procedure TWeighingStationForm.BitBtn4Click(Sender: TObject);
begin
    tbsLogView.Show;
end;

procedure TWeighingStationForm.BitBtn5Click(Sender: TObject);
begin
    Timer1.Enabled:=False;

    if ReadScaleTr <> nil then
        ReadScaleTr.Terminate;

    if ScanTr <> nil then
        ScanTr.Terminate;

    Application.Terminate;
end;

procedure TWeighingStationForm.BitBtnMainWindowClick(Sender: TObject);
begin
    tbsMainWindow.Show;
end;

procedure TWeighingStationForm.Button4Click(Sender: TObject);
var
    BufferSize: integer;
    vBuffer: TIDBytes;
    i: integer;
    character: char;
    str: string;
begin

    with dmMain do
    begin

         BufferSize:=IdTCPClient1.IOHandler.RecvBufferSize;
         IdTCPClient1.IOHandler.ReadBytes(vBuffer, BufferSize, False);

         for i := 0 to BufferSize-1 do
         begin
            character:= char(vBuffer[i]);

            if (character <> #13) or (character <> #10) then
            begin
                str:=str+ Character;
            end
            else
            begin
                if character = #13 then
                begin
                    MemoScale.Lines.Add(str);
                    str:='';
                end;
            end;
         end;

    end;
end;


procedure TWeighingStationForm.chkBoxDebugClick(Sender: TObject);
begin
    if chkBoxDebug.Checked then
    begin
        PageControl1.TabHeight:=18;
        PageControl1.TabWidth:=30;
    end
    else
    begin
        PageControl1.TabHeight:=1;
        PageControl1.TabWidth:=1;
    end;

end;

procedure TWeighingStationForm.DBEdit1Enter(Sender: TObject);
begin
    with dmMain do
    begin
      if FDQStation.RecordCount = 0  then
          FDQStation.Insert;
    end;
end;

procedure TWeighingStationForm.EditBarCodeKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (Key = #13) then
    begin
        HandleBarecode(EditBarCode.Text);
        EditBarCode.Text:='';
    end;

end;

procedure TWeighingStationForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Timer1.Enabled:=False;

    if ReadScaleTr <> nil then
        ReadScaleTr.Terminate;

    if ScanTr <> nil then
        ScanTr.Terminate;
end;

procedure TWeighingStationForm.FormCreate(Sender: TObject);
begin
    PageControl1.ActivePage:= tbsMainWindow;
    PageControl1.TabPosition:=tpRight;
    PageControl1.TabHeight:=1;
    PageControl1.TabWidth:=1;
    SetLabels;
end;

procedure TWeighingStationForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if Key = VK_F12 then
    begin
        spdBtnConnectClick(nil);
        Key:=0;
    end;

    if dmMain.WeightState = wsConnected then
    begin
        if not EditBarCode.Focused then
           EditBarCode.SetFocus;
    end;

    {if dmMain.WeightState = wsConnected then
    begin
        if Key = VK_Return then
        begin
            HandleBarecode(EditBarCode.Text);
            EditBarCode.Text:='';
        end;

        EditBarCode.Text:=EditBarCode.Text+CHAR(Key);
    end;
    }
end;

function TWeighingStationForm.GetBarCodeType(Barcode: string): TBarcodeType;
var
    FirstChar: string;
begin
    FirstChar:=copy(Barcode,0,1);

    if FirstChar = '1' then
       result:=btBox
    else if FirstChar = '3' then
       result:=btWorker
    else
       result:=btUnknown;
end;


procedure TWeighingStationForm.HandleBarecode(Barcode: string);
var
    BarCodeType: TBarcodeType;
begin
    ScreenMessage('','',False);
    // Remove first char
    BarCode:=BarcodeFirstChar(BarCode);

    // Check barcode type box or worker
    BarCodeType:= GetBarCodeType(Barcode);

    if dmMain.Params.ScanWorker = false then
    begin
        if BarCodeType = btWorker then
        begin
            ScreenMessage('Wrong barcode type.','Need to be a Box barcode.', True);
            EditBarCode.Text:='';
            Exit;
        end;
    end;

    if BarCodeType = btWorker then
    begin
        WorkerInfo:=dmMain.GetWorkerFromBarcode(Barcode);

        if dmMain.Params.ScanWorker = True then
           BarCodeScanned.WorkerScanned:=True;

        LabelWorkerBarCode.Caption:=WorkerInfo.BarCode;
        LabelWorkerName.Caption:=WorkerInfo.Name+' '+WorkerInfo.Surname;
        LabelWorkerScanTime.Caption:=TimeToStr(Time);

        if dmMain.Params.ScanWorker = True then
        begin
           BarCodeScanned.WorkerScanned:=True;

           if BarCodeScanned.AllScanned = True then
           begin
              dmMain.InsertLogData(WorkerInfo, BoxInfo);
              SetLastWeightLabels(WorkerInfo, BoxInfo);

           end;
        end
    end
    else if BarCodeType = btBox then
    begin
        BoxInfo.Weight:=CurrentWeight;
        BoxInfo.BarCode:=Barcode;
        LabelBoxBarcode.Caption:=BoxInfo.BarCode;
        LabelBoxWeight.Caption:=FloatToStrF(BoxInfo.Weight,ffFixed,8,3);
        LabelBoxScanTime.Caption:=TimeToStr(Time);

        if LabelCurrentWeight.Font.Color = clGreen then
           BoxInfo.WeightState:='ST'
        else
           BoxInfo.WeightState:='US';

        if dmMain.Params.ScanWorker = True then
        begin
           BarCodeScanned.BoxScanned:=True;

           if BarCodeScanned.AllScanned = True then
           begin
              dmMain.InsertLogData(WorkerInfo, BoxInfo);
              SetLastWeightLabels(WorkerInfo, BoxInfo);
           end;
        end
        else
        begin
           WorkerInfo.Reset;
           dmMain.InsertLogData(WorkerInfo, BoxInfo);
           SetLastWeightLabels(WorkerInfo, BoxInfo);
        end;
    end
    else
    begin
       ScreenMessage('Barcode unknown.', 'Please try again', True);
    end;

       //inputString:='';
       EditBarCode.Text:='';
       Beep;
end;

procedure TWeighingStationForm.ScreenMessage(Line1, Line2: string;
  msgError: Boolean);
begin
    LabelInfo1.Caption:=Line1;
    LabelInfo2.Caption:=Line2;

    if (length(Line1) > 12) then
       LabelInfo1.Font.Height:=50
    else if (length(Line1) > 9) then
       LabelInfo1.Font.Height:=70
    else
       LabelInfo1.Font.Height:=90;

    if msgError = True then
    begin
       LabelInfo1.Font.Color:=clRed;
       LabelInfo2.Font.Color:=clRed;
    end
    else
    begin
       LabelInfo1.Font.Color:=clGreen;
       LabelInfo2.Font.Color:=clGreen;
    end;
end;

function TWeighingStationForm.SetComPort: Boolean;
begin
    with dmMain do
    begin
       ComPort1.DeviceName:=Params.ScannerPort;
       ComPort1.CustomBaudRate:=params.ScannerBaudRate;
       ComPort1.Open;

       result:= ComPort1.Active;
    end;
end;

procedure TWeighingStationForm.SetLabels;
begin
    LabelStationID.Caption:=IntToStr(dmMain.Params.StationID);
    LabelStationName.Caption:=dmMain.Params.StationName;
    GrpBoxWorkerInfo.Visible:= dmMain.Params.ScanWorker;
end;

procedure TWeighingStationForm.SetLastWeightLabels(WorkerInfo: TWorkerInfo; BoxInfo: TBoxInfo);
begin
    try
      BarCodeScanned.Reset;
      LabelLastWeight.Caption:=FloatToStrF(BoxInfo.Weight, ffFixed,8,3);
      LabelLastWeigtTime.Caption:=TimeToStr(Now);
      LabelLastBox.Caption:=BoxInfo.BarCode;

      if dmMain.Params.ScanWorker then
          LabelLastWorker.Caption:=WorkerInfo.Name+' '+WorkerInfo.Surname;

      ScreenMessage('Scan OK', '',False);
      WorkerInfo.Reset;
      BoxInfo.Reset;
    except
        LabelLastWeight.Caption:='0.000';
    end;
end;

procedure TWeighingStationForm.SetCurrentWeightLabels(Weight: Extended; WeightState: string);
begin
    LabelCurrentWeight.Caption:=FloatToStrF(Weight, ffFixed, 6,3);
    LabelCurrWeigtTime.Caption:=TimeToStr(Time);

    if WeightState = 'ST' then
       LabelCurrentWeight.Font.Color:=clGreen
    else
       LabelCurrentWeight.Font.Color:=clRed;
end;

procedure TWeighingStationForm.spdBtnConnectClick(Sender: TObject);
begin
    spdBtnConnect.Glyph.Assign(nil);
    BarCodeScanned.Reset;
    {if dmMain.IdTCPClient1.Connected then
    begin
       dmMain.idTcpClient1.IOHandler.InputBuffer.clear;
       dmMain.IdTCPClient1.Disconnect;
       dmMain.ImageList1.GetBitmap(1, spdBtnConnect.Glyph);
    end
    else
    begin
        if dmMain.ConnectToScale = true then
           dmMain.ImageList1.GetBitmap(0, spdBtnConnect.Glyph)
        else
           dmMain.ImageList1.GetBitmap(1, spdBtnConnect.Glyph);
    end;
    }
    if dmMain.WeightState = wsDisConnected then
    begin
        dmMain.WeightState := wsConnected;
        //dmMain.Params.ScaleIP:='192.168.2.206';
        //dmMain.Params.ScaleIPPort:=23;
        ReadScaleTr:=TReadScaleThread.Create(dmMain.Params);
        dmMain.ImageList1.GetBitmap(1, spdBtnConnect.Glyph);

        with dmMain do
        begin
           if not ((Params.ScannerPort = '') or (Params.ScannerPort = 'COM0')) then
           begin
               if SetComPort = True then
               begin
                    //ComPort1.Open;
                    ScanTr:=TScannerThread.Create(ComPort1);
               end
               else
                  ScreenMessage('','Could not connect to barcode scanner.', True);
           end;
        end;
    end
    else
    begin
        Timer1.Enabled:=False;
        dmMain.WeightState:= wsDisConnected;
        dmMain.ImageList1.GetBitmap(0, spdBtnConnect.Glyph);

        if ReadScaleTr <> nil then
            ReadScaleTr.Terminate;

        if ScanTr <> nil then
            ScanTr.Terminate;
    end;
end;

procedure TWeighingStationForm.Timer1Timer(Sender: TObject);
begin
    if ScaleLineID = LastScaleLineID then
    begin
        Timer1.Enabled:=False;
        try
            ReadScaleTr.Terminate;
        except

        end;

        ReadScaleTr:=TReadScaleThread.Create(dmMain.Params);
    end;

    LastScaleLineID:= ScaleLineID;
end;

function TBarCodeScanned.AllScanned: Boolean;
begin
    if (BoxScanned = True) and (WorkerScanned = True) then
       result:=True
    else
       result:=False;
end;

procedure TBarCodeScanned.Reset;
begin
    BoxScanned:=False;
    WorkerScanned:=False;
    WeighingStationForm.LabelWorkerName.Caption:='.....';
    WeighingStationForm.LabelWorkerBarCode.Caption:='.....';
    WeighingStationForm.LabelWorkerScanTime.Caption:='.....';
    WeighingStationForm.LabelBoxBarcode.Caption:='.....';
    WeighingStationForm.LabelBoxWeight.Caption:='.....';
    WeighingStationForm.LabelBoxScanTime.Caption:='.....';
end;


end.
